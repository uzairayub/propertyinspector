package com.xeminent.propertyinspector;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spnAddress, spnInspector;
    private TextView tvDate, btnNext;
    private ProgressBar progressBar;
    private Calendar myCalendar = Calendar.getInstance();
    private JsonArray addresses, inspectors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initial();
    }

    private void initial()
    {
        progressBar = findViewById(R.id.progressBar);
        spnAddress = findViewById(R.id.spnAddress);
        spnInspector = findViewById(R.id.spnInspector);
        tvDate = findViewById(R.id.tvDate);
        btnNext = findViewById(R.id.btnNext);

        tvDate = findViewById(R.id.tvDate);
        tvDate.setOnClickListener(this);
        btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        loadAddresses();
    }

    private void loadAddresses() {
        Ion.with(MainActivity.this)
                .load("http://new.statementconverter.com/rental_management/index.php/api/get_properties")
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e == null) {
                    if (result.get("result_code").getAsInt() == 200) {
                        addresses = result.get("properties").getAsJsonArray();
                        ArrayList<String> list = new ArrayList<>();
                        list.add("Select property...");
                        for (int i = 0; i < addresses.size(); i++) {
                            JsonObject object = addresses.get(i).getAsJsonObject();
                            list.add(object.get("address").getAsString());
                        }
                        setSalarySpinnerAdapter(spnAddress, list);
                        loadInspectors();
                    }
                }
            }
        });
    }

    private void loadInspectors() {
        Ion.with(MainActivity.this)
                .load("http://new.statementconverter.com/rental_management/index.php/api/get_inspectors")
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e == null) {
                    if (result.get("result_code").getAsInt() == 200) {
                        inspectors = result.get("managers").getAsJsonArray();
                        ArrayList<String> list = new ArrayList<>();
                        for (int i = 0; i < inspectors.size(); i++) {
                            JsonObject object = inspectors.get(i).getAsJsonObject();
                            list.add(object.get("fullname").getAsString());
                        }
                        setSalarySpinnerAdapter(spnInspector, list);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });
    }


    private void setSalarySpinnerAdapter(Spinner mySpinner, ArrayList<String> list) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(spinnerArrayAdapter);
    }

    DatePickerDialog.OnDateSetListener datelistner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            month = month + 1;
            String days = String.format("%02d", day);
            String months = String.format("%02d", month);
            tvDate.setText(year + "-" + months + "-" + days+" "+getCurrentTime());
        }
    };

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tvDate:
            {
                new DatePickerDialog(MainActivity.this, datelistner, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            }
            case R.id.btnNext:
            {
                if(!validation())
                    break;
                JsonObject object = addresses.get(spnAddress.getSelectedItemPosition()).getAsJsonObject();
                String propertyId = object.get("id").getAsString();

                object = inspectors.get(spnInspector.getSelectedItemPosition()).getAsJsonObject();
                String inspectorId = object.get("id").getAsString();

                Intent intent = new Intent(MainActivity.this, AddInspection.class);
                intent.putExtra("property_id", propertyId);
                intent.putExtra("inspector_id", inspectorId);
                intent.putExtra("inspection_date", tvDate.getText().toString());
                startActivity(intent);
                break;
            }
        }
    }


    public String getCurrentTime() {
        Date date = new Date();
        String strDateFormat = "HH:mm:ss";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        return formattedDate;

    }

    private boolean validation()
    {
        if(spnAddress.getSelectedItem().toString().equalsIgnoreCase("Select property..."))
        {
            Toast.makeText(MainActivity.this, "Please select property", Toast.LENGTH_LONG).show();
            return false;
        }
        if(tvDate.getText().toString().isEmpty())
        {
            Toast.makeText(MainActivity.this, "Please select Inspection date", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
