package com.xeminent.propertyinspector;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.xeminent.propertyinspector.Adapters.AdapterImages;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import dmax.dialog.SpotsDialog;
import id.zelory.compressor.Compressor;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import pl.aprilapps.easyphotopicker.EasyImage;


public class AddInspection extends AppCompatActivity implements View.OnClickListener, EasyImage.Callbacks {

    
    private Button btnSave;
    private TextView tvAddImage;

    private RecyclerView rvImages;
    private AdapterImages adapter;
    private ArrayList<File> list;
    private AlertDialog dialog;
    private RadioGroup rgEntry, rgWalkway, rgDoors, rgDriveway, rgGarage, rgWindows, rgWalls, rgSiding, rgPaint, rgFencing, rgRoof,
            rgLawn, rgTrees, rgRetainWalls, rgExtlights, rgMailbox, rgPool, rgPlayarea, rgDebris, rgVehicles, rgPets;
    private EditText etRecommendations, etEntry, etWalkway, etDoors, etDriveway, etGarage, etWindows, etWalls, etSiding, etPaint,
            etFencing, etRoof, etLawn, etTrees, etRetainWalls, etExtlights, etMailbox, etPool, etPlayarea, etDebris, etVehicles, etPets;
    private ArrayList<Integer> DataAdapterClassList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_inspection);
        initial(savedInstanceState);

    }

    private void initial(Bundle savedInstanceState)
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dialog = new SpotsDialog(AddInspection.this, R.style.dialog_style);
        rgEntry = findViewById(R.id.rgEntry);
        rgWalkway = findViewById(R.id.rgWalkway);
        rgDoors = findViewById(R.id.rgDoors);
        rgDriveway = findViewById(R.id.rgDriveway);
        rgGarage = findViewById(R.id.rgGarage);
        rgWindows = findViewById(R.id.rgWindows);
        rgWalls = findViewById(R.id.rgWalls);
        rgSiding = findViewById(R.id.rgSiding);
        rgPaint = findViewById(R.id.rgPaint);
        rgFencing = findViewById(R.id.rgFencing);
        rgRoof = findViewById(R.id.rgRoof);
        rgLawn = findViewById(R.id.rgLawn);
        rgTrees = findViewById(R.id.rgTrees);
        rgRetainWalls = findViewById(R.id.rgRetainWalls);
        rgExtlights = findViewById(R.id.rgExtlights);
        rgMailbox = findViewById(R.id.rgMailbox);
        rgPool = findViewById(R.id.rgPool);
        rgPlayarea = findViewById(R.id.rgPlayarea);
        rgDebris = findViewById(R.id.rgDebris);
        rgVehicles = findViewById(R.id.rgVehicles);
        rgPets = findViewById(R.id.rgPets);

        etRecommendations = findViewById(R.id.etRecommendations);
        etEntry = findViewById(R.id.etEntry);
        etWalkway = findViewById(R.id.etWalkway);
        etDoors = findViewById(R.id.etDoors);
        etDriveway = findViewById(R.id.etDriveway);
        etGarage = findViewById(R.id.etGarage);
        etWindows = findViewById(R.id.etWindows);
        etWalls = findViewById(R.id.etWalls);
        etSiding = findViewById(R.id.etSiding);
        etPaint = findViewById(R.id.etPaint);
        etFencing = findViewById(R.id.etFencing);
        etRoof = findViewById(R.id.etRoof);
        etLawn = findViewById(R.id.etLawn);
        etTrees = findViewById(R.id.etTrees);
        etRetainWalls = findViewById(R.id.etRetainWalls);
        etExtlights = findViewById(R.id.etExtlights);
        etMailbox = findViewById(R.id.etMailbox);
        etPool = findViewById(R.id.etPool);
        etPlayarea = findViewById(R.id.etPlayarea);
        etDebris = findViewById(R.id.etDebris);
        etVehicles = findViewById(R.id.etVehicles);
        etPets = findViewById(R.id.etPets);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        tvAddImage = findViewById(R.id.tvAddImage);
        tvAddImage.setOnClickListener(this);

        rvImages = findViewById(R.id.rvImages);
        rvImages.setLayoutManager(new LinearLayoutManager(AddInspection.this,LinearLayoutManager.HORIZONTAL,false));
        list = new ArrayList<>();
        adapter = new AdapterImages(list);
        rvImages.setAdapter(adapter);

        if(savedInstanceState != null)
        {
            list.addAll((Collection<? extends File>) savedInstanceState.getSerializable("list"));
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == btnSave.getId())
        {
            saveInspection();
        }
        else if(v.getId() == tvAddImage.getId())
        {
            if(cameraPermission())
            {
                EasyImage.openCamera(AddInspection.this, 0);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, this);
    }

    private boolean cameraPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            {
                return true;
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},101);
            return false;
        }
        return true;
    }

    private void saveInspection()
    {
        dialog.show();
        Ion.with(AddInspection.this)
                .load("http://new.statementconverter.com/rental_management/index.php/api/add_inspection")
                .setBodyParameter("property_id", getIntent().getExtras().getString("property_id"))
                .setBodyParameter("inspector_id", getIntent().getExtras().getString("inspector_id"))
                .setBodyParameter("inspection_date", getIntent().getExtras().getString("inspection_date"))
                .setBodyParameter("entry", getselectedString(rgEntry))
                .setBodyParameter("entry_notes", etEntry.getText().toString())
                .setBodyParameter("walkway", getselectedString(rgWalkway))
                .setBodyParameter("walkway_notes", etWalkway.getText().toString())
                .setBodyParameter("doors", getselectedString(rgDoors))
                .setBodyParameter("doors_notes", etDoors.getText().toString())
                .setBodyParameter("driveway", getselectedString(rgDriveway))
                .setBodyParameter("driveway_notes", etDriveway.getText().toString())
                .setBodyParameter("garage", getselectedString(rgGarage))
                .setBodyParameter("garage_notes", etGarage.getText().toString())
                .setBodyParameter("windows", getselectedString(rgWindows))
                .setBodyParameter("windows_notes", etWindows.getText().toString())
                .setBodyParameter("walls", getselectedString(rgWalls))
                .setBodyParameter("walls_notes", etWalls.getText().toString())
                .setBodyParameter("siding", getselectedString(rgSiding))
                .setBodyParameter("siding_notes", etSiding.getText().toString())
                .setBodyParameter("paint", getselectedString(rgPaint))
                .setBodyParameter("paint_notes", etPaint.getText().toString())
                .setBodyParameter("fencing", getselectedString(rgFencing))
                .setBodyParameter("fencing_notes", etFencing.getText().toString())
                .setBodyParameter("roof", getselectedString(rgRoof))
                .setBodyParameter("roof_notes", etRoof.getText().toString())
                .setBodyParameter("lawn", getselectedString(rgLawn))
                .setBodyParameter("lawn_notes", etLawn.getText().toString())
                .setBodyParameter("trees", getselectedString(rgTrees))
                .setBodyParameter("trees_notes", etTrees.getText().toString())
                .setBodyParameter("retainwalls", getselectedString(rgRetainWalls))
                .setBodyParameter("retainwalls_notes", etRetainWalls.getText().toString())
                .setBodyParameter("extlights", getselectedString(rgExtlights))
                .setBodyParameter("extlights_notes", etExtlights.getText().toString())
                .setBodyParameter("mailbox", getselectedString(rgMailbox))
                .setBodyParameter("mailbox_notes", etMailbox.getText().toString())
                .setBodyParameter("pool", getselectedString(rgPool))
                .setBodyParameter("pool_notes", etPool.getText().toString())
                .setBodyParameter("playarea", getselectedString(rgPlayarea))
                .setBodyParameter("playarea_notes", etPlayarea.getText().toString())
                .setBodyParameter("debris", getselectedString(rgDebris))
                .setBodyParameter("debris_notes", etDebris.getText().toString())
                .setBodyParameter("vehicles", getselectedString(rgVehicles))
                .setBodyParameter("vehicles_notes", etVehicles.getText().toString())
                .setBodyParameter("pets", getselectedString(rgPets))
                .setBodyParameter("pets_notes", etPets.getText().toString())
                .setBodyParameter("recommendations", etRecommendations.getText().toString())
                .asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result)
            {
                if(e == null)
                {
                    if(result.get("result_code").getAsInt() == 200)
                    {
                        Toast.makeText(AddInspection.this, result.get("message").getAsString(), Toast.LENGTH_LONG).show();
                        if(list.size() > 0)
                        {
                            uploadImages(result.get("new_inspection_id").getAsInt());
                            Toast.makeText(AddInspection.this, "Uploading Images", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            finish();
                        }
                    }
                }
                else
                {
                    dialog.dismiss();
                    Toast.makeText(AddInspection.this, "Network error Please\ntry again", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void uploadImages(int new_inspection_id)
    {

        List <Part> files = new ArrayList();
        for (int i = 0; i < list.size(); i++)
        {
            compressFile(list.get(i), i);
            files.add(new FilePart("fileToUpload[" + i + "]", list.get(i)));
        }

        Ion.with(AddInspection.this)
                .load("http://new.statementconverter.com/rental_management/index.php/api/add_inspection_photos")
                .setMultipartParameter("inspection_id", new_inspection_id+"")
                .addMultipartParts(files)
                .asJsonObject()
                .setCallback(new FutureCallback < JsonObject > ()
                {
                    @Override
                    public void onCompleted(Exception e, JsonObject result)
                    {
                        dialog.dismiss();
                        if(e == null)
                        {
                            Toast.makeText(AddInspection.this, result.get("message").getAsString(), Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else
                        {
                            Log.d("response", e.toString());
                        }
                    }
                });
    }

    private void compressFile(File file, final int position)
    {
        new Compressor(this)
                .compressToFileAsFlowable(file)
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<File>() {
                    @Override
                    public void accept(File myfile)
                    {
                        list.add(position, myfile);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable)
                    {
                        throwable.printStackTrace();
                    }
                });
    }
    private String getselectedString(RadioGroup radioGroup)
    {
        int id = 0;
        try
        {
            id = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = findViewById(id);
            if(radioButton.getText().toString().equalsIgnoreCase("okay"))
            {
                id = 1;
            }
            else
            {
                id = 2;
            }
        }
        catch (Exception e)
        {
            id = 0;
        }

        return id+"";
    }

    @Override
    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type)
    {
        Log.d("","");
    }

    @Override
    public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type)
    {
        list.add(imageFiles.get(0));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCanceled(EasyImage.ImageSource source, int type) {

        Log.d("","");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("list", list);
    }
}
